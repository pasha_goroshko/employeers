﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Models
{
    public class PositionModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование должности
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Базовый оклад
        /// </summary>
        public decimal Salary { get; set; }

        public override string ToString()
        {
            return $"{Title}[{Salary}]";
        }
    }
}
