﻿using Employees.Models.Enums;
using System;
using System.Collections.Generic;

namespace Employees.Models
{
    public class EmployeeModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FirdName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Паспорт
        /// </summary>
        public PassportModel Passport { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public SexModel Sex { get; set; }

        /// <summary>
        /// Образование
        /// </summary>
        public List<EducationModel> Educations { get; set; }

        /// <summary>
        /// Список контрактов
        /// </summary>
        public List<ContractModel> Contracts { get; set; }
    }
}
