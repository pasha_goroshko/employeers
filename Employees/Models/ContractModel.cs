﻿using System;

namespace Employees.Models
{
    public class ContractModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public PositionModel Position { get; set; }
        public int Position_Id { get; set; }

        /// <summary>
        /// Надбавка к зарплате
        /// </summary>
        public decimal Increase { get; set; }

        /// <summary>
        /// С
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// По
        /// </summary>
        public DateTime? Till { get; set; }

        /// <summary>
        /// Дополнительная информация по контракту
        /// </summary>
        public string AdditionalyInfo { get; set; }
    }
}
