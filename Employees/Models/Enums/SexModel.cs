﻿namespace Employees.Models.Enums
{
    public enum SexModel
    {
        None,

        /// <summary>
        /// Мужской
        /// </summary>
        Male,

        /// <summary>
        /// Женский
        /// </summary>
        Female
    }
}
