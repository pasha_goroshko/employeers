﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Models.Enums
{
    public enum RoleModel
    {
        None,
        User,
        Admin
    }
}
