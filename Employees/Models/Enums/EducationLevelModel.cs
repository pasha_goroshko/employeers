﻿namespace Employees.Models.Enums
{
    public enum EducationLevelModel
    {
        None,

        /// <summary>
        /// Базовое
        /// </summary>
        Basic,

        /// <summary>
        /// Среднее
        /// </summary>
        Average,

        /// <summary>
        /// Высшее
        /// </summary>
        Higher
    }
}
