﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Models
{
    public class UserBlockModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Заблокирован с
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// Заблокирован по
        /// </summary>
        public DateTime? Till { get; set; }

        /// <summary>
        /// Является ли блокировка активной(Для ручного вывода пользователя из блокировки)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Причина блокировки
        /// </summary>
        public string Reason { get; set; }
    }
}
