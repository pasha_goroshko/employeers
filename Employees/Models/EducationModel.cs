﻿using Employees.Models.Enums;
using System;

namespace Employees.Models
{
    public class EducationModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Уровень образования
        /// </summary>
        public EducationLevelModel EducationLevel { get; set; }

        /// <summary>
        /// Дата квалификации
        /// </summary>
        public DateTime QualificationDate { get; set; }

        /// <summary>
        /// Квалификация
        /// </summary>
        public string Qualification { get; set; }

        /// <summary>
        /// Учреждение
        /// </summary>
        public string Institution { get; set; }

        public override string ToString()
        {
            return Qualification;
        }
    }
}
