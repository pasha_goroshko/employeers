﻿using Employees.Models.Enums;
using System.Collections.Generic;

namespace Employees.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Роль пользователя
        /// </summary>
        public RoleModel Role { get; set; }

        /// <summary>
        /// Является ли пользователь заблокирован
        /// </summary>
        public bool IsBlocked { get; set; }

        public string Blocked => IsBlocked ? "Разблокировать" : "Заблокировать";

        public List<UserBlockModel> UserBlocks { get; set; }

        public UserBlockModel UserBlock { get; set; }
    }
}
