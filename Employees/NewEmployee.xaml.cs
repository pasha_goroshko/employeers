﻿using Employees.Context;
using Employees.Domain;
using Employees.Models;
using Employees.Models.Enums;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for NewEmployee.xaml
    /// </summary>
    public partial class NewEmployee : Window
    {
        private readonly EmployeeModel employee;
        public NewEmployee()
        {
            InitializeComponent();

            employee = new EmployeeModel
            {
                Passport = new PassportModel(),
                Contracts = new List<ContractModel>(),
                Educations = new List<EducationModel>()
            };
            PrepareData();
        }

        private void PrepareData()
        {
            using (var context = new EmployeesContext())
            {
                var positions = context.Positions.ToList().Adapt<List<PositionModel>>();
                positionComboBox.ItemsSource = positions;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NewEducationWindow educationWindow = new NewEducationWindow(employee);
            educationWindow.ShowDialog();

            aducationListBox.ItemsSource = null;
            aducationListBox.ItemsSource = employee.Educations;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (positionComboBox.SelectedItem is PositionModel position)
            {
                employee.FirstName = firstNameTextBox.Text.Trim();
                employee.LastName = lastNameTextBox.Text.Trim();
                employee.FirdName = firdNameTextBox.Text.Trim();
                employee.Sex = maleRadioButton.IsChecked.Value ? SexModel.Male : SexModel.Female;

                employee.Passport.ActiveFrom = passportActiveFrom.SelectedDate.Value.Date;
                employee.Passport.Issued = passportGetBy.Text.Trim();
                employee.Passport.Number = passportNumber.Text.Trim();

                var contract = new ContractModel
                {
                    Position_Id = position.Id,
                    AdditionalyInfo = contractInfo.Text.Trim(),
                    From = contractActiveFrom.SelectedDate.Value,
                    Till = contractActiveTill.SelectedDate
                };

                employee.Contracts.Add(contract);

                using (var context = new EmployeesContext())
                {
                    context.Employees.Add(employee.Adapt<Employee>());
                    context.SaveChanges();

                    MessageBox.Show("Сотрудник сохранен!");
                    this.Close();
                }
            }

        }
    }
}
