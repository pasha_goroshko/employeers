﻿using Employees.Context;
using Employees.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Логика взаимодействия для PositionCreateWindow.xaml
    /// </summary>
    public partial class PositionCreateWindow : Window
    {
        public PositionCreateWindow()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {

            }
            else
            {
                e.Handled = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(positionSalary.Text))
                MessageBox.Show("Необходимо указать базовую зарплату");
            else if (string.IsNullOrWhiteSpace(positionName.Text))
                MessageBox.Show("Необходимо указать наименование должности");
            else
            {
                var position = new Position
                {
                    Salary = decimal.Parse(positionSalary.Text.Trim()),
                    Title = positionName.Text.Trim()
                };
                using (var context = new EmployeesContext())
                {
                    if (context.Positions.Any(x => x.Title.Equals(position.Title, StringComparison.OrdinalIgnoreCase)))
                        MessageBox.Show("Должность с таким именем уже существует!");
                    else
                    {
                        context.Positions.Add(position);
                        context.SaveChanges();

                        MessageBox.Show("Должность успешно добавлена!");
                        this.Close();
                    }
                }
            }
        }
    }
}
