﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            LogInWindow logInWindow = new LogInWindow();
            logInWindow.ShowDialog();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NewEmployee newEmployeeWindow = new NewEmployee();
            newEmployeeWindow.ShowDialog();
        }

        private void userMenuAdminOnly_Click(object sender, RoutedEventArgs e)
        {
            if (Helpers.AuthorizationData.User.Role == Models.Enums.RoleModel.Admin)
            {
                UserControlWindow userControlWindow = new UserControlWindow();
                userControlWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("Доступ запрещен администратором!", "Ошибка доступа!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void locksHistoryAdminOnly_Click(object sender, RoutedEventArgs e)
        {
            if (Helpers.AuthorizationData.User.Role == Models.Enums.RoleModel.Admin)
            {
                BlockUserWindow blockUserWindow = new BlockUserWindow();
                blockUserWindow.ShowDialog();
            }
            else
            {
                BlockUserWindow blockUserWindow = new BlockUserWindow(Helpers.AuthorizationData.User.Id);
                blockUserWindow.ShowDialog();
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            PositionWindow positionWindow = new PositionWindow();
            positionWindow.ShowDialog();
        }
    }
}
