﻿using Employees.Context;
using Employees.Models;
using Employees.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for BlockUserWindow.xaml
    /// </summary>
    public partial class BlockUserWindow : Window
    {

        int? _userId = null;
        public BlockUserWindow(int? userId = null)
        {
            InitializeComponent();

            this._userId = userId;

            ReloadData();
        }

        private void ReloadData()
        {
            using (var context = new EmployeesContext())
            {
                var users = context.Users.Where(x => _userId.HasValue ? x.Id == _userId : true).ToList();

                var userBlockingInfo = users.SelectMany(x => x.UserBlocks.Select(v => new UserModel
                {
                    Name = x.Name,
                    Login = x.Login,
                    Role = (RoleModel)x.Role,
                    IsBlocked = x.IsBlocked,
                    UserBlock = new UserBlockModel
                    {
                        From = v.From,
                        IsActive = v.IsActive,
                        Reason = v.Reason,
                        Till = v.Till
                    }
                }));

                userBlockingListView.ItemsSource = userBlockingInfo;
            }
        }
    }
}
