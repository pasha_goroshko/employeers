﻿using Employees.Context;
using Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for NewUserWindow.xaml
    /// </summary>
    public partial class NewUserWindow : Window
    {
        UserModel userModel = new UserModel
        {
            Role = Models.Enums.RoleModel.User
        };
        public NewUserWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((sender as CheckBox).IsChecked.Value)
                userModel.Role = Models.Enums.RoleModel.Admin;
            else
                userModel.Role = Models.Enums.RoleModel.User;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            userModel.Name = nameTextBox.Text;
            userModel.Login = loginTextBox.Text;
            userModel.Password = passwordBox.Password;

            if (string.IsNullOrWhiteSpace(userModel.Name))
                MessageBox.Show( "Необходимо указать имя пользователя!", "Ошибка данных!", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (string.IsNullOrWhiteSpace(userModel.Login))
                MessageBox.Show("Необходимо указать логин!", "Ошибка данных!", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (string.IsNullOrWhiteSpace(userModel.Password))
                MessageBox.Show("Необходимо указать пароль!", "Ошибка данных!", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                using (var context = new EmployeesContext())
                {
                    if (context.Users.Any(x => x.Login == userModel.Login))
                        MessageBox.Show($"Пользователь с логином '{userModel.Login}' уже существует!", "Ошибка добавления!", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                    {
                        context.Users.Add(new Domain.User
                        {
                            Login = userModel.Login,
                            Name = userModel.Name,
                            Password = userModel.Password,
                            Role = (Domain.Role)userModel.Role
                        });

                        context.SaveChanges();

                        this.Close();
                    }
                }
            }
        }
    }
}
