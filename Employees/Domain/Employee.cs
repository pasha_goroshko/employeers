﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string FirdName { get; set; }
        
        /// <summary>
        /// Дата рождения
        /// </summary>
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Паспорт
        /// </summary>
        public virtual Passport Passport { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// Образование
        /// </summary>
        public virtual ICollection<Education> Educations { get; set; }

        /// <summary>
        /// Список контрактов
        /// </summary>
        public virtual ICollection<Contract> Contracts { get; set; }
    }

    public enum Sex
    {
        None,

        /// <summary>
        /// Мужской
        /// </summary>
        Male,

        /// <summary>
        /// Женский
        /// </summary>
        Female
    }
}
