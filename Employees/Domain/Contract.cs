﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    public class Contract
    {
        public int Id { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public virtual Position Position { get; set; }
        public int Position_Id { get; set; }

        /// <summary>
        /// Надбавка к зарплате
        /// </summary>
        public decimal Increase { get; set; }

        /// <summary>
        /// С
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// По
        /// </summary>
        public DateTime? Till { get; set; }

        /// <summary>
        /// Дополнительная информация по контракту
        /// </summary>
        public string AdditionalyInfo { get; set; }
    }
}
