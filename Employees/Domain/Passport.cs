﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    public class Passport
    {
        public int Id { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime ActiveFrom { get; set; }

        /// <summary>
        /// Паспорт выдан
        /// </summary>
        public string Issued { get; set; }
    }
}
