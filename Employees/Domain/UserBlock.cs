﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    public class UserBlock
    {
        public int Id { get; set; }

        /// <summary>
        /// Заблокирован с
        /// </summary>
        [Required]
        public DateTime From { get; set; }

        /// <summary>
        /// Заблокирован по
        /// </summary>
        public DateTime? Till { get; set; }

        /// <summary>
        /// Является ли блокировка активной(Для ручного вывода пользователя из блокировки)
        /// </summary>
        public bool IsActive { get; set; }

        public virtual User User { get; set; }

        /// <summary>
        /// Причина блокировки
        /// </summary>
        [Required]
        public string Reason { get; set; }
    }
}
