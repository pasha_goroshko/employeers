﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    /// <summary>
    /// Должность
    /// </summary>
    public class Position
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование должности
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Базовый оклад
        /// </summary>
        [Required]
        public decimal Salary { get; set; }

        /// <summary>
        /// Список договоров
        /// </summary>
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
