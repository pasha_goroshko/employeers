﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Роль пользователя
        /// </summary>
        [Required]
        public Role Role { get; set; }

        /// <summary>
        /// Является ли пользователь заблокирован
        /// </summary>
        [Required]
        public bool IsBlocked { get; set; }

        public virtual ICollection<UserBlock> UserBlocks { get; set; }
    }

    public enum Role
    {
        None,
        User,
        Admin
    }
}
