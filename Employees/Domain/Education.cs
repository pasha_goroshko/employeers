﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Domain
{
    /// <summary>
    /// Образование
    /// </summary>
    public class Education
    {
        public int Id { get; set; }

        /// <summary>
        /// Уровень образования
        /// </summary>
        public EducationLevel EducationLevel { get; set; }

        /// <summary>
        /// Дата квалификации
        /// </summary>
        public DateTime QualificationDate { get; set; }

        /// <summary>
        /// Квалификация
        /// </summary>
        public string Qualification { get; set; }

        /// <summary>
        /// Учреждение
        /// </summary>
        public string Institution { get; set; }
    }

    public enum EducationLevel
    {
        None,

        /// <summary>
        /// Базовое
        /// </summary>
        Basic,

        /// <summary>
        /// Среднее
        /// </summary>
        Average,

        /// <summary>
        /// Высшее
        /// </summary>
        Higher
    }
}
