﻿using Employees.Models;
using Employees.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for NewEducationWindow.xaml
    /// </summary>
    public partial class NewEducationWindow : Window
    {
        private EmployeeModel _employee;

        public string[] Levels { get; set; }
        public NewEducationWindow(EmployeeModel employee)
        {
            InitializeComponent();
            this._employee = employee;

            Levels = new[] { "Базовое", "Среднее", "Высшее" };

            employeeTextBox.Text = $"{employee.FirstName} {employee.LastName} {employee.FirdName}";
            educLevelComboBox.ItemsSource = Levels;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            EducationModel education = new EducationModel
            {
                EducationLevel = (EducationLevelModel)(educLevelComboBox.SelectedIndex + 1),
                Institution = instituteNameTextBox.Text.Trim(),
                Qualification = qualificationTextBox.Text.Trim(),
                QualificationDate = datePicker.SelectedDate.Value.Date
            };

            _employee.Educations.Add(education);
            this.Close();
        }
    }
}
