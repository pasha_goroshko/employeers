﻿namespace Employees.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAttributes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Login", c => c.String(nullable: false));
            AddColumn("dbo.Users", "Password", c => c.String(nullable: false));


            Sql("INSERT INTO [dbo].[Users] ([Name],[Role],[IsBlocked],[Login],[Password]) VALUES (N'Administrator', 2, 'FALSE', N'admin', N'admin')");

        }

        public override void Down()
        {
            DropColumn("dbo.Users", "Password");
            DropColumn("dbo.Users", "Login");
        }
    }
}
