﻿namespace Employees.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Increase = c.Decimal(nullable: false, precision: 18, scale: 2),
                        From = c.DateTime(nullable: false),
                        Till = c.DateTime(),
                        AdditionalyInfo = c.String(),
                        Employee_Id = c.Int(),
                        Position_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Employee_Id)
                .ForeignKey("dbo.Positions", t => t.Position_Id)
                .Index(t => t.Employee_Id)
                .Index(t => t.Position_Id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        FirdName = c.String(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Sex = c.Int(nullable: false),
                        Passport_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Passports", t => t.Passport_Id)
                .Index(t => t.Passport_Id);
            
            CreateTable(
                "dbo.Educations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EducationLevel = c.Int(nullable: false),
                        QualificationDate = c.DateTime(nullable: false),
                        Qualification = c.String(),
                        Institution = c.String(),
                        Employee_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Employee_Id)
                .Index(t => t.Employee_Id);
            
            CreateTable(
                "dbo.Passports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        ActiveFrom = c.DateTime(nullable: false),
                        Issued = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Salary = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserBlocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        From = c.DateTime(nullable: false),
                        Till = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                        Reason = c.String(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Role = c.Int(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserBlocks", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Contracts", "Position_Id", "dbo.Positions");
            DropForeignKey("dbo.Employees", "Passport_Id", "dbo.Passports");
            DropForeignKey("dbo.Educations", "Employee_Id", "dbo.Employees");
            DropForeignKey("dbo.Contracts", "Employee_Id", "dbo.Employees");
            DropIndex("dbo.UserBlocks", new[] { "User_Id" });
            DropIndex("dbo.Educations", new[] { "Employee_Id" });
            DropIndex("dbo.Employees", new[] { "Passport_Id" });
            DropIndex("dbo.Contracts", new[] { "Position_Id" });
            DropIndex("dbo.Contracts", new[] { "Employee_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.UserBlocks");
            DropTable("dbo.Positions");
            DropTable("dbo.Passports");
            DropTable("dbo.Educations");
            DropTable("dbo.Employees");
            DropTable("dbo.Contracts");
        }
    }
}
