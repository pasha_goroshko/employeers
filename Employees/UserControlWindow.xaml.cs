﻿using Employees.Context;
using Employees.Helpers;
using Employees.Models;
using Employees.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for UserControlWindow.xaml
    /// </summary>
    public partial class UserControlWindow : Window
    {
        public UserControlWindow()
        {
            InitializeComponent();

            ReloadData();
        }

        private void ReloadData()
        {
            using (var context = new EmployeesContext())
            {
                // Находим в БД пользователя программы по логину и паролю
                var users = context.Users
                    .Select(x => new UserModel
                    {
                        Id = x.Id,
                        IsBlocked = x.UserBlocks.Any(v => v.IsActive),
                        Login = x.Login,
                        Name = x.Name,
                        Password = x.Password,
                        Role = (RoleModel)x.Role
                    })
                    .ToArray();

                userListView.ItemsSource = null;
                userListView.ItemsSource = users;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            var user = (sender as Hyperlink).DataContext as UserModel;

            if (AuthorizationData.User?.Id == user.Id)
            {
                MessageBox.Show( "Невозможно удалить свой аккаунт. Обратитесь к вашему администратору.", "Ошибка удаления!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                using (var context = new EmployeesContext())
                {
                    var userDb = context.Users.FirstOrDefault(x => x.Id == user.Id);
                    context.Users.Remove(userDb);
                    context.SaveChanges();

                    MessageBox.Show( "Пользователь успешно удален!", "Успешное удаление.", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

            ReloadData();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NewUserWindow newUserWindow = new NewUserWindow();
            newUserWindow.ShowDialog();

            ReloadData();
        }

        private void Hyperlink_Click_1(object sender, RoutedEventArgs e)
        {
            var user = (sender as Hyperlink).DataContext as UserModel;

            BlockUserWindow blockUserWindow = new BlockUserWindow(user.Id);
            blockUserWindow.ShowDialog();
        }

        private void Hyperlink_Click_2(object sender, RoutedEventArgs e)
        {
            var user = (sender as Hyperlink).DataContext as UserModel;

            using (var context = new EmployeesContext())
            {
                var userDb = context.Users.FirstOrDefault(x => x.Id == user.Id);

                if (userDb.UserBlocks.Any(x => x.IsActive))
                {
                    var locks = userDb.UserBlocks.Where(x => x.IsActive);

                    foreach (var @lock in locks)
                    {
                        @lock.IsActive = false;
                        @lock.Till = DateTime.Now;
                        userDb.IsBlocked = false;

                        context.UserBlocks.Attach(@lock);
                        context.Entry(@lock).State = EntityState.Modified;
                    }
                }
                else
                {
                    userDb.IsBlocked = true;

                    userDb.UserBlocks.Add(new Domain.UserBlock
                    {
                        User = userDb,
                        From = DateTime.Now,
                        IsActive = true,
                        Reason = "Заблокирован администратором."
                    });

                    context.Users.Attach(userDb);
                    context.Entry(userDb).State = EntityState.Modified;
                }

                context.SaveChanges();

                MessageBox.Show("Операция успешно выполнена!", "Успешно", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            ReloadData();
        }
    }
}
