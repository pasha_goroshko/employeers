﻿using Employees.Context;
using Employees.Helpers;
using Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Логика взаимодействия для PositionWindow.xaml
    /// </summary>
    public partial class PositionWindow : Window
    {
        public PositionWindow()
        {
            InitializeComponent();
            ReloadData();
        }
        private void ReloadData()
        {
            using (var context = new EmployeesContext())
            {
                var positions = context.Positions
                    .Select(x => new PositionModel
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Salary = x.Salary
                    })
                    .ToArray();

                positionListView.ItemsSource = null;
                positionListView.ItemsSource = positions;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            var position = (sender as Hyperlink).DataContext as PositionModel;

            if (AuthorizationData.User?.Role != Models.Enums.RoleModel.Admin)
            {
                MessageBox.Show("Обратитесь к администратору!", "Ошибка удаления!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                using (var context = new EmployeesContext())
                {
                    var positionDb = context.Positions.FirstOrDefault(x => x.Id == position.Id);

                    if (positionDb.Contracts.Any())
                    {
                        MessageBox.Show("У данной должности имеются прикрепленные договора!", "Ошибка удаления!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        context.Positions.Remove(positionDb);
                        context.SaveChanges();

                        MessageBox.Show("Должность успешно удалена!", "Успешное удаление.", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }

            ReloadData();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PositionCreateWindow positionCreateWindow = new PositionCreateWindow();
            positionCreateWindow.ShowDialog();
            ReloadData();
        }
    }
}
