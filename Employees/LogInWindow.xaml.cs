﻿using Employees.Context;
using Employees.Helpers;
using Employees.Models;
using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaction logic for LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        public LogInWindow()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            // Выходим полностью из приложения
            Environment.Exit(0);
        }

        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new EmployeesContext())
            {
                // Находим в БД пользователя программы по логину и паролю
                var user = context.Users.FirstOrDefault(x => x.Login == userName.Text && x.Password == secData.Password);

                // Если нашли пользователя
                if (user != null)
                {
                    if (user.IsBlocked)
                    {
                        var @lock = user.UserBlocks.FirstOrDefault(x => x.IsActive);

                        MessageBox.Show($"Пользователь заблокирован. \nДата блокировки: {@lock.From} ", "Принудительная блокировка.", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        // Сохраняем пользователя, записываем в статическую переменную
                        AuthorizationData.User = user.Adapt<UserModel>();

                        // Закрываем окно авторизации
                        this.Close();
                    }
                }
                // Если не нашли пользователя, выдаем сообщение, что логин или пароль не верный
                else
                    MessageBox.Show("Вы ввели неверный логин или пароль!");
            }
        }
    }
}
