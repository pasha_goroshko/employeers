﻿using Employees.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Employees.Context
{
    public class EmployeesContext : DbContext
    {
        public EmployeesContext()
            : base("EmployeesDbContext")
        {
            // в окне Output будут видны запросы к БД, которые генерирует Entity Framework
            this.Database.Log = s => Debug.Write(s);
        }

        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserBlock> UserBlocks { get; set; }

        public virtual DbSet<Passport> Passports { get; set; }
        public virtual DbSet<Education> Educations { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
    }
}
